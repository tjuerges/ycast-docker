FROM python:3-slim

WORKDIR /opt/ycast
EXPOSE 48010

RUN cd /opt/ycast && \
python3 -m pip install --upgrade --prefer-binary pip wheel && \
python3 -m pip install ycast && \
python3 -m pip cache purge

ENTRYPOINT ["python3", "-m", "ycast", "-l", "0.0.0.0", "-p", "48010", "-c", "/opt/ycast/stations/ycast-stations.yaml"]
