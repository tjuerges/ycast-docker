#! /usr/bin/env bash

if [[ ${OS} = Darwin ]]; then
    # Export the radio stations from Cuterdio.
    defaults read -app Cuterdio &>/dev/null
    if [ ${?} -eq 0 ]; then
        defaults export -app Cuterdio Cuterdio.plist
        python3 ./convert_cuterdio_stations.py
        rm -f Cuterdio.plist
    fi

    # Export the radio stations from Eter Radio.
    defaults read -app 'Eter Radio' &>/dev/null
    if [ ${?} -eq 0 ]; then
        python3 ./convert_eter_radio_stations.py
    fi
else
    echo "Sorry but the available conversions scripts currently only work on macOS."
fi
