#! /usr/bin/env bash

export YCAST_STATIONS=${PWD}/stations
docker_image=registry.gitlab.com/tjuerges/ycast-docker:$(<VERSION)
run_this="run --rm -it -v${YCAST_STATIONS}:/opt/ycast/stations ${docker_image}"

function ycast_venv()
{
    [[ ! -d .venv ]] && { python3 -m venv --upgrade-deps .venv; }
    . .venv/bin/activate
    python_version=$(python3 -V|cut -d' ' -f2)
    [[ ! -d .venv/lib/python${python_version}/site-packages/ycast ]] && { python3 -m pip --require-virtualenvinstall ycast; }
    python3 -m ycast -p 48010 -c ${YCAST_STATIONS}/ycast-stations.yaml
    deactivate
}

podman ${run_this} || docker ${run_this} || ycast_venv
