#! /usr/bin/env python3

from os import getenv
from sqlite3 import connect

db = f'{getenv("HOME")}/Library/Containers/com.mackozer.NewWorldRadioPlayer/Data/Library/Application Support/Eter Radio/EterDataModel.sqlite'
db_conn = connect(db)
crsr = db_conn.cursor()
result = crsr.execute("select ZTITLE,ZURL from ZMDBSTATION;")
stations = result.fetchall()
result.close()
crsr.close()
stations.sort()

yaml_string = 'Favoriten:\n'
for station in stations:
    name = station[0]
    url = station[1]
    yaml_string += str(f'  {name}: {url}\n')

with open('stations/eter_radio-ycast-stations.yaml', 'w') as file:
    file.write(yaml_string)
