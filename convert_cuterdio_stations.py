#! /usr/bin/env python3

from json import loads
from plistlib import load

with open('Cuterdio.plist', 'rb') as file:
    plist = plistlib.load(file)

stations = json.loads(plist['SETTING_STATIONS'])

yaml_string = 'Favoriten:\n'
for station in stations:
    name = station['Name']
    url = station['Url']
    yaml_string += str(f'  {name}: {url}\n')

with open('stations/cuterdio-ycast-stations.yaml', 'w') as file:
    file.write(yaml_string)
